<?php

namespace Scio\core\app\console;

use Scio\di\configuration\ScioDIConfigurator;
use Yii;
use yii\console\Application;


class ScioApplication extends Application{
	public function __construct( $config = [] ){
		parent::__construct( $config );
		
		$diConfigurer = new ScioDIConfigurator();
		
		$diConfigurer->configureYiiDI( Yii::$container );
	}
}
