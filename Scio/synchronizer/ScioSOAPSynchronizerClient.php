<?php

namespace Scio\synchronizer;

use Exception;
use Scio\synchronizer\dto\ParamObjectProvider;
use Scio\synchronizer\dto\SOAPResult;
use SoapClient;
use yii\base\Object;


class ScioSOAPSynchronizerClient extends Object implements SOAPFileSynchronizer, SOAPSessionProvider{
	private $sessionID;
	private $login;
	private $password;
	private $soapClient;
	
	
	public function __construct( $login, $password, $passwordHashAlgo, $wsdl, $config = [] ){
		$this->soapClient = new SoapClient( $wsdl, [ 'cache_wsdl' => WSDL_CACHE_NONE, 'features' => SOAP_SINGLE_ELEMENT_ARRAYS, 'trace' => TRUE, 'exceptions' => TRUE ] );
		$this->login = $login;
		$this->password = hash( $passwordHashAlgo, $password );
		
		parent::__construct( $config );
	}
	
	public function getSessionID(){
		if( $this->sessionID === NULL ){
			$response = new SOAPResult( $this->soapClient->createSession( $this->login, $this->password ) );

			if( $response->errorCode === 0 ){
				$this->sessionID = $response->value;
			}
		}
		
		if( $this->sessionID === NULL ){
			throw new Exception( "Cannot create session ID, invalid login or password?" );
		}
		
		return $this->sessionID;
	}
	
	public function synchronizeFile( ParamObjectProvider $file ){
		return new SOAPResult( $this->soapClient->synchronizeFile( $this->getSessionID(), $file->getAsParamObject() ) );
	}

	

}
