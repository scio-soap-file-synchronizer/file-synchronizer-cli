<?php

namespace Scio\synchronizer;


interface SOAPSessionProvider{
	/**
	 * @return string Provides session id to use with webservice server
	 */
	public function getSessionID();
}
