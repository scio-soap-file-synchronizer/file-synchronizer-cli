<?php

namespace Scio\synchronizer\dto;


class SOAPResult{
	const ID_RESULT_SUCCESS = 0;
	const ID_RESULT_ERROR = 1;
	
	
	public $errorCode;
	public $errorMessage;
	public $value;
	
	
	public function __construct( $response ){
		$this->errorCode = (int) $response->errorCode;
		$this->errorMessage = $response->errorMessage;
		$this->value = $response->value;
	}
}
