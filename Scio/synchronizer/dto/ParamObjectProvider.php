<?php

namespace Scio\synchronizer\dto;

interface ParamObjectProvider{
	public function getAsParamObject();
}
