<?php

namespace Scio\synchronizer\dto;

use Scio\file\SyncFileInfo;
use stdClass;


class FileParam implements ParamObjectProvider{
	public $data;
	public $fileName;
	public $filePath;
	
	
	function __construct( $data, $fileName, $filePath ){
		$this->data = $data;
		$this->fileName = $fileName;
		$this->filePath = $filePath;
	}

	public static function createFromSyncFileInfo( SyncFileInfo $syncFileInfo ){
		$data = file_get_contents( $syncFileInfo->getFileNameWithPath() );
		
		$fileParam = new static( $data, $syncFileInfo->getFileName(), $syncFileInfo->getRelativeFilePath() );
		
		unset( $data );
		
		return $fileParam;
	}
	
	public function getAsParamObject(){
		$stdClass = new stdClass();
		
		$stdClass->fileName = $this->fileName;
		$stdClass->filePath = $this->filePath;
		$stdClass->data = base64_encode( $this->data );
		
		return $stdClass;
	}
}
