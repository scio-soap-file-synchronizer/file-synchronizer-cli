<?php

namespace Scio\synchronizer;

use Scio\synchronizer\dto\ParamObjectProvider;
use Scio\synchronizer\dto\SOAPResult;


interface SOAPFileSynchronizer{
	/**
	 * @param ParamObjectProvider $file
	 * @return SOAPResult result of synchronization
	 */
	public function synchronizeFile( ParamObjectProvider $file );
}
