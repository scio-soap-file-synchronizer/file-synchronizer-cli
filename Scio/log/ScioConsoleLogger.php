<?php

namespace Scio\log;

use Exception;
use yii\helpers\Console;


class ScioConsoleLogger implements Logger{
	const LOG_LEVEL_ERROR = 2;
	const LOG_LEVEL_INFO = 0;
	const LOG_LEVEL_SUCCESS = 1;

	private $consoleHelper;
	
	
	public function __construct(){
		$this->consoleHelper = new Console();
	}
	
	private function logInternal( $message, $level ){
		$color = '%w';
		
		if( $level === static::LOG_LEVEL_ERROR ){
			$color = '%R';
		}elseif( $level === static::LOG_LEVEL_SUCCESS ){
			$color = '%G';
		}
		
		$message = "{$color} {$message}";
		
		$this->consoleHelper->output( $this->consoleHelper->renderColoredString( $message ) );
	}
	
	public function logError( $message ){
		$this->logInternal( $message, static::LOG_LEVEL_ERROR );
	}

	public function logException( Exception $ex ){
		$this->logInternal( \yii\helpers\VarDumper::export( $ex ), static::LOG_LEVEL_ERROR );
	}

	public function logInfo( $message ){
		$this->logInternal( $message, static::LOG_LEVEL_INFO );
	}

	public function logSuccess( $message ){
		$this->logInternal( $message, static::LOG_LEVEL_SUCCESS );
	}

}
