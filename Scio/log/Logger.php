<?php

namespace Scio\log;

use Exception;


interface Logger{
	public function logInfo( $message );
	public function logSuccess( $message );
	public function logError( $message );
	public function logException( Exception $ex );
}
