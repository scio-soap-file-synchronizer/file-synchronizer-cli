<?php

namespace Scio\di\configuration;

use Scio\file\ScioFileListProvider;
use Scio\file\service\ScioFileSynchronizationService;
use Scio\file\service\SynchronizationFlagResolver;
use Scio\file\SyncFileListProvider;
use Scio\log\Logger;
use Scio\log\ScioConsoleLogger;
use Scio\synchronizer\ScioSOAPSynchronizerClient;
use Scio\synchronizer\SOAPFileSynchronizer;
use Yii;
use yii\di\Container;


class ScioDIConfigurator{
	private function getDIConfigration(){
		$configuration = [
			SyncFileListProvider::class => ScioFileListProvider::class,
			SynchronizationFlagResolver::class => ScioFileSynchronizationService::class,
			Logger::class => ScioConsoleLogger::class
		];

		return $configuration;
	}

	public function configureYiiDI( Container $container ){
		$diConfiguration = $this->getDIConfigration();
		
		foreach( $diConfiguration as $src => $target ){
			$container->set( $src, $target );
		}
		
		$container->setSingleton( SOAPFileSynchronizer::class, ScioSOAPSynchronizerClient::class, [ Yii::$app->params[ 'Ws.Sync.Login' ], Yii::$app->params[ 'Ws.Sync.Password' ], Yii::$app->params[ 'Ws.Sync.Password.HashAlgo' ] , Yii::$app->params[ 'Ws.Sync.URL' ] ] );
	}

}
