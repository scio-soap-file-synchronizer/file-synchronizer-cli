<?php

namespace Scio\file;

use RecursiveDirectoryIterator;
use RecursiveIteratorIterator;
use Scio\file\service\SynchronizationFlagResolver;
use Scio\file\SyncFileListProvider;
use yii\base\Object;


class ScioFileListProvider extends Object implements SyncFileListProvider{
	const EXCLUDE_DIR_DOT_FILE_NAME = '.php-sync-excluded';
	
	private $synchronizationFlagResolver;

	/**
	 * @var SyncFileInfo[] 
	 */
	private $allSyncFileInfos;

	/**
	 * @var SyncFileInfo[]
	 */
	private $syncFileInfos;
	private $excludedFilesByFilePath = [];
	
	
	public function __construct( SynchronizationFlagResolver $synchronizationFlagResolver, $config = [] ){
		$this->synchronizationFlagResolver = $synchronizationFlagResolver;

		parent::__construct( $config );
	}

	public function getAllFileList( $path ){
		if( $this->allSyncFileInfos === NULL ){
			$rii = new RecursiveIteratorIterator( new RecursiveDirectoryIterator( $path, RecursiveDirectoryIterator::SKIP_DOTS ) );

			$files = [ ];
			
			foreach( $rii as $fileInfo ){
				if( !$fileInfo->isDir() ){
					if( !$this->getIsDirExcluded( (string) $fileInfo->getPathInfo() ) ){
						$files[] = new SyncFileInfo( $fileInfo->getFilename(), $fileInfo->getPathInfo(), $path );
					}
				}
			}

			$this->allSyncFileInfos = $files;
		}

		return $this->allSyncFileInfos;
	}

	public function getFileListToSynchronize( $path ){
		if( $this->syncFileInfos === NULL ){
			$allSyncFileInfos = $this->getAllFileList( $path );
			$syncFileInfos = [ ];

			foreach( $allSyncFileInfos as $syncFileInfo ){
				if( !$this->synchronizationFlagResolver->resolveIsSynchronized( $syncFileInfo ) ){
					$syncFileInfos[] = $syncFileInfo;
				}
			}

			$this->syncFileInfos = $syncFileInfos;
		}

		return $this->syncFileInfos;
	}

	// @todo iteracyjne sprawdzanie każdego z podkatalogów.
	private function getIsDirExcluded( $dirFilePath ){
		if( array_key_exists( $dirFilePath, $this->excludedFilesByFilePath ) ){
			return $this->excludedFilesByFilePath[ $dirFilePath ];
		}
		
		$result = file_exists( $dirFilePath . DIRECTORY_SEPARATOR . static::EXCLUDE_DIR_DOT_FILE_NAME );
		
		$this->excludedFilesByFilePath[ $dirFilePath ] = $result;
		
		return $result;
	}

}
