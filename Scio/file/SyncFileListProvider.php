<?php

namespace Scio\file;


interface SyncFileListProvider{
	/**
	 * @param string $path
	 * @return SyncFileInfo[]
	 */
	public function getAllFileList( $path );
	
	/**
	 * @param string $path
	 * @return SyncFileInfo[]
	 */
	public function getFileListToSynchronize( $path ); 
}
