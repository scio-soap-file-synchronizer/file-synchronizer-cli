<?php

namespace Scio\file;


class SyncFileInfo{
	private $fileName;
	private $filePath;
	private $md5Sum;
	private $basePath;
	
	
	function __construct( $fileName, $filePath, $basePath ){
		$this->fileName = $fileName;
		$this->filePath = $filePath;
		$this->basePath = $basePath;
		
		$this->md5Sum = md5_file( $filePath . DIRECTORY_SEPARATOR . $fileName );
	}
	
	public function getFileName(){
		return $this->fileName;
	}

	public function getFilePath(){
		return $this->filePath;
	}

	public function getMd5Sum(){
		return $this->md5Sum;
	}

	public function getFileNameWithPath(){
		return $this->filePath . DIRECTORY_SEPARATOR . $this->fileName;
	}
	
	public function getIsDir(){
		return $this->isDir;
	}
	
	public function getRelativeFilePath(){
		return str_replace( $this->basePath, '', $this->filePath );
	}

}
