<?php

namespace Scio\file\service;

use Scio\file\SyncFileInfo;
use Yii;


class ScioFileSynchronizationService implements SynchronizationFlagResolver{
	const KEY_PREFIX = 'sync_';
	
	private $cache;
	
	
	public function __construct(){
		$this->cache = Yii::$app->getCache();
	}
	
	private function getCacheKey( SyncFileInfo $syncFileInfo ){
		return static::KEY_PREFIX . $syncFileInfo->getFileNameWithPath() . $syncFileInfo->getMd5Sum();
	}
	
	public function resolveIsSynchronized( SyncFileInfo $syncFileInfo ){
		return $this->cache->exists( $this->getCacheKey( $syncFileInfo ) );
	}

	public function setIsSynchronized( SyncFileInfo $syncFileInfo, $flag ){
		if( $flag ){
			$this->cache->add( $this->getCacheKey( $syncFileInfo ), $this->getCacheKey( $syncFileInfo  ) );
		}else{
			if( $this->resolveIsSynchronized( $syncFileInfo ) ){
				$this->cache->delete( $this->getCacheKey( $syncFileInfo ) );
			}
		}
	}

}
