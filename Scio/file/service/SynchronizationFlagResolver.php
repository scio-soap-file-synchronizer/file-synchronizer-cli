<?php

namespace Scio\file\service;

use Scio\file\SyncFileInfo;


interface SynchronizationFlagResolver{
	public function resolveIsSynchronized( SyncFileInfo $syncFileInfo );
	
	public function setIsSynchronized( SyncFileInfo $syncFileInfo, $flag );
}
