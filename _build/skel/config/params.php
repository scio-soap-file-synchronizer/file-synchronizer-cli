<?php

return [
	'Ws.Sync.URL' => 'PATH_TO_/index.php?r=soap/synchronizer',
	'Ws.Sync.Login' => 'SECURITY_LOGIN',
	'Ws.Sync.Password' => 'SECURITY_PASSWORD',
	'Ws.Sync.Password.HashAlgo' => 'sha256'
];
