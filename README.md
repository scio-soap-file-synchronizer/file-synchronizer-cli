# file-synchronizer-cli

This is small backup program created with Yii2 framework (mainly used for DI).
It provides synchronization of yours files by SOAP methods.

For automatic backup you can always write simple cron file and put it inside your crontab.

You need to write your own SOAP server, or check another project from this group for example.
Your WS should implement following classess and methods:
```
class FileParam{
	/**
	 * @var string
	 * @soap
	 */
	public $data;
	
	/**
	 * @var string
	 * @soap 
	 */
	public $fileName;
	
	/**
	 * @var string
	 * @soap
	 */
	public $filePath;
}


class SOAPResult{
	/**
	 * Error code 0 means success. Otherwise there was an error.
	 * @var int
	 * @soap
	 */
	public $errorCode;
	
	/**
	 * @var string
	 * @soap
	 */
	public $errorMessage;
	
	/**
	 * @var string
	 * @soap 
	 */
	public $value;
}
```

```
/**
 * @return SOAPResult
 */
public function synchronizeFile( $sessionID, FileParam $fileParam );

/**
 * @return SOAPResult
 */
public function createSession( $login, $password );
```
Before sending FileParam the data itself is encoded by base64 algorithm.

Just copy config data from _build/skel, edit it properly and you are good to go.

## Example usage:
Directories which have **.php-sync-excluded** 
dot file inside are excluded. Currently thy is **not** recursive.

To synchronize content of directory type (-p stands for --path parameter): 
> ./scio filesync/synchronize -p ~/Dokumenty

If you would like to resynchronize all of previously synchronized files just flush cache by cli:
> ./scio cache/flush-all

