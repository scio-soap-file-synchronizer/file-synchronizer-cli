<?php

namespace app\commands;

use Exception;
use Scio\file\service\SynchronizationFlagResolver;
use Scio\file\SyncFileListProvider;
use Scio\log\Logger;
use Scio\synchronizer\dto\FileParam;
use Scio\synchronizer\dto\SOAPResult;
use Scio\synchronizer\SOAPFileSynchronizer;
use yii\console\Controller;
use const PHP_EOL;


class FilesyncController extends Controller{
	/**
	 * @var string
	 * Path to directory which should be synchronized
	 */
	public $path;
	private $syncFileListProvider;
	private $logger;
	private $soapFileSynchronizer;
	private $synchronizationFlagResolver;

	
	public function __construct( $id, $module, SyncFileListProvider $syncFileListProvider, Logger $logger, SOAPFileSynchronizer $soapFileSynchronizer, SynchronizationFlagResolver $synchronizationFlagResolver, $config = [] ){
		$this->syncFileListProvider = $syncFileListProvider;
		$this->logger = $logger;
		$this->soapFileSynchronizer = $soapFileSynchronizer;
		$this->synchronizationFlagResolver = $synchronizationFlagResolver;

		parent::__construct( $id, $module, $config );
	}

	public function options( $actionID ){
		if( $actionID === 'synchronize' ){
			return [ 'path' ];
		}

		return [ ];
	}

	public function optionAliases(){
		return [ 'p' => 'path' ];
	}

	/**
	 * Synchronize files by WS
	 */
	public function actionSynchronize( $path ){
		try{
			$files = $this->syncFileListProvider->getFileListToSynchronize( $path );

			foreach( $files as $file ){
				$message = 'Synchronizing: ' . $file->getFileName() . PHP_EOL . $file->getFilePath() . PHP_EOL . $file->getMd5Sum();

				$this->logger->logInfo( $message );

				$soapResult = $this->soapFileSynchronizer->synchronizeFile( FileParam::createFromSyncFileInfo( $file ) );

				if( $soapResult->errorCode === SOAPResult::ID_RESULT_SUCCESS ){
					$this->logger->logSuccess( "Success" );

					$this->synchronizationFlagResolver->setIsSynchronized( $file, TRUE );
				}else{
					$this->synchronizationFlagResolver->setIsSynchronized( $file, FALSE );
					
					$this->logger->logError( 'WS Error:' . $soapResult->errorMessage );
				}
			}
		}catch( Exception $ex ){
			$this->logger->logException( $ex );
		}
	}

}
